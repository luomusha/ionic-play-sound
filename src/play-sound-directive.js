/**
 * Created by Lossa on 2017/1/4.
 * 使用方法：
 * <play-sound url="http://www.example.com/url/sound/test.ogg" side="right"></play-sound>
 * url指向资源的url，side可选项
 */
angular.module('play-sound.directive', [])
    .directive('playSound', ['$state', '$timeout', '$rootScope',
        function ($state, $timeout, $rootScope) {
            return {
                restrict: 'E',
                replace: true,
                scope: {
                    url: '@url',//必填
                    side: '@side',// 左边还是右边
                    isNew: '@isNew',// 红点显示
                },
                templateUrl: 'play-sound.html',
                link: function (scope, element, attr, controller) {
                    if (scope.side == 'right') {
                        element[0].className = 'play-sound play-sound-right'
                    }
                    scope.xhr = new XMLHttpRequest();
                    scope.blob = undefined;
                    scope.src = undefined;
                    scope.isLoading = true;//loading 标志位
                    scope.isError = false;//isError 标志位
                    scope.isPlaying = false;//isPlaying 标志位
                    scope.dur = 0;
                    scope.btn = element[0].getElementsByClassName('button')[0];
                    scope.$on('$ionicParentView.beforeLeave', function (e, v) {
                        $rootScope.player.pause();
                        $rootScope.player.src = null;
                    });
                    scope.$on('$ionicParentView.afterEnter', function (e, v) {
                        if (scope.blob && scope.src) {

                        } else {
                            scope.xhr.open('GET', scope.url);
                            scope.xhr.responseType = 'blob';
                            scope.xhr.send();
                        }
                        console.log('$ionicParentView.afterEnter')
                    });

                    scope.$watch(function () {
                        if ($rootScope.player) {
                            return $rootScope.player.src;
                        }
                        return undefined;

                    }, function (newVal, oldVal) {
                        scope.isPlaying = (newVal == scope.src);
                    });

                    if (!$rootScope.player) {
                        $rootScope.player = new window.Audio();
                        $rootScope.player.addEventListener('ended', function () {
                            $timeout(function () {
                                $rootScope.player.src = '';
                            });
                        })
                    }

                    scope.play = function () {
                        scope.isNew = false;
                        if ($rootScope.player.src == scope.src) {
                            $rootScope.player.pause();
                            $rootScope.player.src = null;
                        } else {
                            $rootScope.player.src = scope.src;
                            $rootScope.player.play();
                        }
                    };

                    scope.reload = function () {
                        scope.xhr.open('GET', scope.url);
                        scope.xhr.responseType = 'blob';
                        scope.xhr.send();
                        scope.isLoading = true;
                        scope.isError = false;
                    };

                    scope.xhr.onreadystatechange = function () {
                        if (this.readyState == 4) {
                            if (this.status == 200) {
                                $timeout(function () {
                                    scope.isLoading = false;
                                    scope.isError = false;
                                });
                                scope.src = window.URL.createObjectURL(this.response);
                                scope.blob = this.response;
                                console.log(scope.blob);
                                scope.dur = this.response.size / 1024 / 10;
                                scope.btn.style.width = this.response.size / 1024 + 'px';
                            } else if (this.status != 200) {
                                console.log('bad end');
                                $timeout(function () {
                                    scope.isError = true;
                                });
                            }
                        }
                    };

                }
            }
        }]);

angular.module('play-sound.filter', [])
    .filter('DurFilter', [function () {
        return function (time) {
            if (time) {
                if (isNaN(time)) {
                    time = '?';
                }
                return Math.round(time) + '\"';
            }
        };
    }])
    .filter('Trusted', ['$sce', function ($sce) {
        return function (url) {
            return $sce.trustAsResourceUrl(url);
        };
    }]);
;