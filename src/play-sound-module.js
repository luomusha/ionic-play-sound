/**
 * Created by Lossa on 2017/1/4.
 */
angular.module('play-sound', [
    'ionic',
    'play-sound.filter',
    'play-sound.directive',
    'play-sound.templates',
]);